/***
 * Rather usess less.
 * It animates the box containing the welcome text and buttons to go from one corner to the middle in 200 iterations. It will take at a total 1000 ms (200 iterations times 5.)
 */


const reference = document.getElementById('animateMe')

let topPx = (Math.random()*100) < 50 ? 0 :  (window.innerHeight - reference.offsetHeight)
let leftPx = ((Math.random()*100) < 50 ? 0 : (window.innerWidth - reference.offsetWidth))
const direction = {top: topPx > 0 ? -1 : 1, left: leftPx > 0 ? -1 : 1};

const middleHeight = window.innerHeight/2 - (reference.offsetHeight/2)
const middleWidth = window.innerWidth/2 - (reference.offsetWidth/2)

let topMultiplier = Math.abs(topPx - middleHeight) / 200
let leftMultiplier = Math.abs(leftPx - middleWidth) / 200
reference.style.left = leftPx + 'px'
reference.style.top = topPx + 'px'
reference.style.opacity = 1
const intervalId = setInterval(animation, 5)

function animation() {
    let top = topPx
    let left = leftPx
    if(compareValues(topPx, middleHeight, direction.top > 0 ? '<' : '>')) {
        topPx += (direction.top * topMultiplier)
        reference.style.top = topPx + 'px'
    }
    if(compareValues(leftPx, middleWidth, direction.left > 0 ? '<' : '>')) {
        leftPx += (direction.left * leftMultiplier)
        reference.style.left = leftPx + 'px'
    }

    if(top === topPx && left === leftPx) {
        reference.style.position = "relative"
        reference.style.top = 0;
        reference.style.left = 0 ;
        clearInterval(intervalId)
    }
    

}


function compareValues(value1, value2, type) {
    switch(type) {
        case '<':
            return value1 < value2;
        case '>':
            return value1 > value2;
    }
}

function maxValue(v1, v2) {
    return v1 > v2 ? v1 : v2
}