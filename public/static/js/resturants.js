/**
 * Executes when the body loads, it fetches allthe resturants and embeds them to our html page.
 */
function onLoad() {
    let resturantHolder = document.getElementById("resturants")

    fetch('/data')
    .then((resp) => resp.json())
    .then((json) => {
        json.forEach(resturant => {
            resturantHolder.innerHTML += generateResturant(resturant)
        })
    })
}
/**
 * Used to show the map modual and add a map with the longitude and latidude of the restruan
 * @param {} long 
 * @param {*} lat 
 */
function toggleMap(long, lat) {
    document.getElementById("modual").style.display = "grid"
    document.getElementById("map").innerHTML = ""
    new ol.Map({
        target: 'map',
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          })
        ],
        view: new ol.View({
          center: ol.proj.fromLonLat([long, lat]),
          zoom: 18
        })
      });
    
}
function hideMap() {
    document.getElementById("modual").style.display = "none"
}

const reviews = []
/**
 * Constructs HTML for the resturants and returns it.
 * @param {} resturant - Object containing the resturant info
 */
function generateResturant(resturant) {
    return `<div class="resturant">

        <div class="innerColumn">
            <img class="image" src="${resturant.image}"/>
            <div class="innerRow">
                <span>${resturant.name}</span>
                <span onclick="toggleMap(${resturant.coordinates.long},${resturant.coordinates.lat})" class="mapLink">${resturant.location}</span>  
            </div>
        </div>
        <p>${resturant.description}</p>
        <div class="innerRow">
            <span>${getStars(resturant.averageRating)}</span>
            <span onclick="renderReviews(${addReviews(resturant.reviews)})">[View Reviews]</span>    
        </div>
    </div>`
}


function getStars(amount) {
    let stars = "" 
    for(let i = 1; i < 6; i++) {
        if(i <= amount) {
            stars += '<img class="filled" src="assets/img/filled.png"/>'
        }
        else {
            stars += '<img class="filled" src="assets/img/empty.png"/>'
        }
    }
    return stars;
}
function addReviews(reviewList) {
    return reviews.push(reviewList) - 1
}
/**
 * Adds HTML for each review in the restrant.
 * @param {*} reviewID 
 */
function renderReviews(reviewID) {
    const reviewsList = document.getElementById("reviewsList") 
    reviewsList.innerHTML = '<button onclick="hideReviews()">[x]</button>'
    reviews[reviewID].forEach(review => {
        reviewsList.innerHTML += generateReview(review)
    })
    document.getElementById("reviews").style.display = "grid"
}
function hideReviews() {
    document.getElementById("reviews").style.display = "none"
}

function generateReview(review) {
    return `<div class="review">
                <h4>${review.author}</h4>
                <p>${review.message}</p>
            </div>`
}