const path = require('path')

const express = require('express')
const app = express()

const {PORT = 8080} = process.env
//Route for static content such as css js and imgaes.
app.use('/assets', express.static(path.join(__dirname, 'public', 'static')))

app.get('/', (req, res) => {
    return res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'))
})

app.get('/data', (req, res) => {
    return res.status(200).sendFile(path.join(__dirname, 'data.json'))
})

app.get('/resturants', (req, res) => {
    return res.status(200).sendFile(path.join(__dirname, 'public', 'resturants.html'))
})

app.listen(PORT, () => console.log('opened'))