## Build an express server that displays some restaurant data.

* Name
* Location
* Description
* An image (stored on the server - no linking!)
* Average rating (1-5 stars)
* Optional: A list of reviews
* The server must have the following enpoints:

1. '/' -> index.html (links to other endpoints, has you name listed)

2. '/data' -> data.json file, sent as json 

3. '/restaurants' -> restaurant.html this page must use css and js (from /static) to fetch the /data file and programmatically display the restaurant information.